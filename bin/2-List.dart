void main() {
  List example = [1, 7, 12, 3, 56, 2, 87, 34, 54];
  print(example.first);
  print(example[4]);
  print(example.last);

  List example2 = [3, 12, 43, 1, 25, 6, 5, 7];
  List example3 = [55, 11, 23, 56, 78, 1, 9];
  example2.add(example3);

  List example4 = [
    'a',
    'd',
    'F',
    'l',
    'u',
    't',
    't',
    'e',
    'R',
    'y',
    '3',
    'b',
    'h',
    'j'
  ];
  print(example4.sublist(2, 9));

  List q = [1, 2, 3, 4, 5, 6, 7];
  print(q.contains(3));
  print(q.first);
  print(q.last);
  print(q.length);

  List a = [601, 123, 2, "dart", 45, 95, "dart24", 1];
  print(a.contains('dart'));
  print(a.contains(951));

  List t = ["post", 1, 0, "flutter"];
  String myDart = 'Flutter';
  myDart.toLowerCase;
  print(myDart.contains('t'));

  List d = ["I", "Started", "Learn", "Flutter", "Since", "April"];
  String myFlutter = '';
  myFlutter = d.join('*');
  print(myFlutter);

  List<int> Qwerty = [1, 9, 3, 195, 202, 2, 5, 7, 9, 10, 3, 15, 0, 11];
  Qwerty.sort((a, b) => a.compareTo(b));
  print(Qwerty);
}
