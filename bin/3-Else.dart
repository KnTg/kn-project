import 'dart:io';

void main(List<String> args) {
  // print('Введите число');
  // final finger = int.parse(stdin.readLineSync()!);
  // switch (finger) {
  //   case 1:
  //     print('Большой палец');
  //     break;
  //   default:
  // }

  int min = int.parse(stdin.readLineSync()!);
  if (min >= 0 && min <= 15) {
    print('Первая четверть');
  } else if (min >= 16 && min <= 30) {
    print('Вторая четверть');
  } else if (min >= 31 && min <= 45) {
    print('Третья четверть');
  } else if (min >= 46 && min <= 60) {
    print('Четвертая четверть');
  }
}
